from notebook.auth import passwd
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("password")
args = parser.parse_args()

print(passwd(args.password))
