#set & hash password

my_pass=$(cat /root/mypass.txt)

if [ -n "$my_pass" ]; then
  source /root/.bashrc
  my_sha=$(python /root/jupyter/pass.py "$my_pass")
  sed -i "s/c.NotebookApp.password.*/c.NotebookApp.password = '$my_sha'/g" /root/.jupyter/jupyter_notebook_config.py
  cd /root/jupyter/shared/
  jupyter notebook --no-browser 
else
  echo "Failure during hashing"
fi
