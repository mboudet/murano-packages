#!/bin/bash

# Get the Node IP string, decode it and add it to /root/cluster/nodes.l

cat /dev/null > /root/cluster/nodes.l
for ip in $(echo "$1" | sed "s/,/ /g")
do
  echo "$ip" >> /root/cluster/nodes.l
  ssh-keyscan "$ip" >> ~/.ssh/known_hosts
done
