#!/bin/bash

#Get Murano user choices and install slurm

#$1 : password
#$2 : Host name
#$3 : Host ip
#$4 : Nodes names(s) (space separated values)
#$5 : Nodes ip(s) (space separated values)

# Prepare Munge
logfile="/root/log_slurm"
echo "Hostname = $2" >> $logfile
echo "HostIP = $3" >> $logfile
echo "NodeIPs = $5" >> $logfile
nodes="$4"
nameList=${nodes::-1}
echo "NodeNames = $nameList" >> $logfile
echo "Install Munge" >> $logfile
apt-get update
apt-get install -y libmunge-dev libmunge2 munge

echo -n "$1" | sha512sum | cut -d' ' -f1 > /etc/munge/munge.key
chown -R munge:munge /etc/munge/
chmod 400 /etc/munge/munge.key
/etc/init.d/munge start
service munge restart

# Prepare Slurm
echo "Install Slurm" >> $logfile
apt-get install -y slurm-wlm slurm-wlm-doc
mkdir /var/spool/slurmd
touch /var/log/slurmd.log
#Get config file and edit it
echo "Edit conf file" >> $logfile
cp conf/slurm.conf /etc/slurm-llnl/slurm.conf
sed -i "s/ControlMachine.*/ControlMachine=$2/g" /etc/slurm-llnl/slurm.conf
sed -i "s/ControlAddr=.*/ControlAddr=$3/g" /etc/slurm-llnl/slurm.conf
for name in $(echo "$4" | sed "s/,/ /g")
do
echo "NodeName=$name NodeAddr=TEMP_IP CPUs=2 State=UNKNOWN" >> /etc/slurm-llnl/slurm.conf
done
echo "PartitionName=main Nodes=$nameList Default=YES MaxTime=8-00:00:00 State=UP" >> /etc/slurm-llnl/slurm.conf
for nodeip in $(echo "$5" | sed "s/,/ /g")
do
awk -v myip="$nodeip" 'NR==1,/TEMP_IP/{sub(/TEMP_IP/,myip)} 1' /etc/slurm-llnl/slurm.conf  > temp.txt && mv -f temp.txt /etc/slurm-llnl/slurm.conf
done
#Launch Slurm
slurmd -D >> /root/log_slurm 2>&1 &
