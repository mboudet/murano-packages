#!/bin/bash

# $1 = Manila share path
# $2 = Expected mount path

logfile="/root/share_log"
echo "Trying to mount $1 in $2" >> $logfile
mkdir -p "$2"

mount "$1" "$2" || echo "Failure mounting $1 in $2" >> $logfile
