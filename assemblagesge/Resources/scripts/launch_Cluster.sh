#!/bin/bash


# Adding SSH agent to simplify cluster setup
echo "SSH agent test"  >> /root/key_logs 2>&1
eval `ssh-agent` >> /root/key_logs 2>&1
ssh-add >> /root/key_logs 2>&1

/ifb/bin/ifb-cluster-sge /root/cluster/nodes.l 2 >> /root/cluster_logs 2>&1
