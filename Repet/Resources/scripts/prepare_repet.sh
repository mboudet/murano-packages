#!/bin/bash

# Prepare configs files

echo "Creating project $1 in /mnt/repet-disk/$1"  >> /root/key_logs 2>&1
mkdir -p "/mnt/repet-disk/$1" >> /root/key_logs 2>&1

export REPET_PATH=/ifb/bin/repet_pipe/


echo "Changing database IP $2 and adding setEnv file in project folder"  >> /root/key_logs 2>&1
cp $REPET_PATH/config/setEnv.sh /mnt/repet-disk/$1/
sed -i 's/\(REPET_HOST=\).*/\1'"$2/g" /mnt/repet-disk/$1/setEnv.sh

if [ ! -e /mnt/repet-disk/$1/TEannot.cfg ]; then
  echo "Add and config teannot file"  >> /root/key_logs 2>&1
  cp $REPET_PATH/config/TEannot.cfg /mnt/repet-disk/$1/
  sed -i 's/\(repet_host:\).*/\1'"$2/g" /mnt/repet-disk/$1/TEannot.cfg
  sed -i 's/\(project_name:\).*/\1'"$1/g" /mnt/repet-disk/$1/TEannot.cfg
  sed -i 's/\(project_dir:\).*/\1'"\/mnt\/repet-disk\/$1/g" /mnt/repet-disk/$1/TEannot.cfg
  sed -i 's/\(BLR_blast:\).*/\1'"wu/g" /mnt/repet-disk/$1/TEannot.cfg
else
  echo "Teannot file found, only changing DB ip"  >> /root/key_logs 2>&1
  sed -i 's/\(repet_host:\).*/\1'"$2/g" /mnt/repet-disk/$1/TEannot.cfg
  sed -i 's/\(BLR_blast:\).*/\1'"wu/g" /mnt/repet-disk/$1/TEannot.cfg
fi

if [ ! -e /mnt/repet-disk/$1/TEdenovo.cfg ]; then
  echo "Adding and config tedenovo file"  >> /root/key_logs 2>&1
  cp $REPET_PATH/config/TEdenovo.cfg /mnt/repet-disk/$1/
  sed -i 's/\(repet_host:\).*/\1'"$2/g" /mnt/repet-disk/$1/TEdenovo.cfg
  sed -i 's/\(project_name:\).*/\1'"$1/g" /mnt/repet-disk/$1/TEdenovo.cfg
  sed -i 's/\(project_dir:\).*/\1'"\/mnt\/repet-disk\/$1/g" /mnt/repet-disk/$1/TEdenovo.cfg
else
  echo "Tedenovo file found, only changing DB ip"  >> /root/key_logs 2>&1
  sed -i 's/\(repet_host:\).*/\1'"$2/g" /mnt/repet-disk/$1/TEdenovo.cfg
fi
