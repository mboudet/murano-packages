#!/bin/bash

PL_USER="$1"

echo "Install Apache"
# echo "Installing all packages."
apt-get update
apt-get install -y apache2
apt-get install -y \
     apt-transport-https \
     ca-certificates \
     curl \
     gnupg2 \
     software-properties-common

ip route get 1 | awk '{print $NF;exit}' > config
MYIP=$(<config)
echo $MYIP | sed -e 's/\./-/g' > config
URLIP=$(<config)
#cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1 > config
openssl rand -hex 3 > config
URLID=$(<config)


a2enmod ldap auth_basic authnz_ldap authz_user proxy proxy_http proxy_wstunnel headers

echo "Setup LDAP rules"
cat <<EOT > /etc/apache2/sites-enabled/000-default.conf
<VirtualHost *:80>
	ServerName che-$URLIP.vm.openstack.genouest.org
	ServerAlias *.openstack-$URLIP.genouest.org
	ServerAdmin webmaster@localhost
	ErrorLog ${APACHE_LOG_DIR}/error.log
	CustomLog ${APACHE_LOG_DIR}/access.log combined
    <Location "/">
    AuthType Basic
    AuthName "Restricted Area"
    AuthBasicProvider ldap
    AuthLDAPURL "ldap://192.168.1.147/ou=People,dc=genouest,dc=org?uid?sub?(objectClass=*)"
        <RequireAll>
        #Require valid-user
        Require ldap-user ${PL_USER}
        </RequireAll>
    </Location>

	ProxyPreserveHost On
    ProxyTimeout 600
    ProxyPass "/wsmaster/websocket" "ws://localhost:8080/wsmaster/websocket"
    ProxyPassReverse "/wsmaster/websocket" "ws://localhost:8080/wsmaster/websocket"
    ProxyPass "/api/ws" "ws://localhost:8080/api/ws"
    ProxyPassReverse "/api/ws" "ws://localhost:8080/api/ws"
    ProxyPass / http://127.0.0.1:8080/
    ProxyPassReverse / http://127.0.0.1:8080/
    Header setifempty "Access-Control-Allow-Origin" "*"
    Header setifempty "Access-Control-Expose-Headers" "JAXRS-Body-Provided"
</VirtualHost>

<VirtualHost *:80>
    ServerName che-$URLID-$URLIP.vm.openstack.genouest.org
    ServerAlias *-$URLID-$URLIP.vm.openstack.genouest.org
    ServerAdmin webmaster@localhost
    ErrorLog /error.log
    CustomLog /access.log combined

    ProxyPreserveHost On
    ProxyTimeout 600
    ProxyPass "/wsmaster/websocket" "ws://localhost:8080/wsmaster/websocket"
    ProxyPassReverse "/wsmaster/websocket" "ws://localhost:8080/wsmaster/websocket"
    ProxyPass "/api/ws" "ws://localhost:8080/api/ws"
    ProxyPassReverse "/api/ws" "ws://localhost:8080/api/ws"

    ProxyPass "/wsagent" "ws://localhost:8080/wsagent"
    ProxyPassReverse "/wsagent" "ws://localhost:8080/wsagent"
    ProxyPass "/connect" "ws://localhost:8080/connect"
    ProxyPassReverse "/connect" "ws://localhost:8080/connect"
    ProxyPass "/pty" "ws://localhost:8080/pty"
    ProxyPassReverse "/pty" "ws://localhost:8080/pty"

    ProxyPass / http://127.0.0.1:8080/
    ProxyPassReverse / http://127.0.0.1:8080/
    Header setifempty "Access-Control-Allow-Origin" "*"
    Header setifempty "Access-Control-Expose-Headers" "JAXRS-Body-Provided"
</VirtualHost>
EOT

systemctl enable apache2
service apache2 restart

# docker
curl -fsSL https://download.docker.com/linux/$(. /etc/os-release; echo "$ID")/gpg | apt-key add -
add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") \
   $(lsb_release -cs) \
   stable"
apt-get update
echo "Installing Docker"
apt-get install -y docker-ce || true
echo "Wait a few seconds"
sleep 20
echo "Starting Che"
# Run che

mkdir -p /opt/che-data
docker run --rm -v /var/run/docker.sock:/var/run/docker.sock -v /opt/che-data:/data -e CHE_SINGLE_PORT=true -e CHE_DOCKER_SERVER__EVALUATION__STRATEGY_CUSTOM_EXTERNAL_PROTOCOL=https -e CHE_DOCKER_SERVER__EVALUATION__STRATEGY_CUSTOM_TEMPLATE="<serverName>-<workspaceId>-$URLID-$URLIP.vm.openstack.genouest.org"  eclipse/che start
#docker run --rm -v /var/run/docker.sock:/var/run/docker.sock -v /opt/che-data:/data -e CHE_SINGLE_PORT=true -e CHE_DOCKER_SERVER__EVALUATION__STRATEGY_CUSTOM_TEMPLATE="<serverName>.<machineName>.<workspaceId>.$URLID.$URLIP.vm.openstack.genouest.org"  eclipse/che start
echo "Done"
